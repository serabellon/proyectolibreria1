﻿using System.ComponentModel.DataAnnotations;

namespace Proyecto_Biblioteca.Models
{
    public class Publication
    {
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string Category { get; set; }
        public string? Identification { get; set; }

        public Publication() { 
            this.Id = Guid.NewGuid();
        }
    }
}
