﻿namespace Proyecto_Biblioteca.Models
{
    public class Magazine : Publication
    {
        public string? Periodicity { get; set; }

        public void GenerateIdentifier()
        {
            Random rnd = new Random();
            String number = rnd.Next(0, 999999).ToString("D13");
            this.Identification = "ISSN " + number;
        }
    }
}
