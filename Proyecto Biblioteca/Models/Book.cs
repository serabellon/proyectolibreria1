﻿using System.Reflection.Emit;

namespace Proyecto_Biblioteca.Models
{
    public class Book : Publication
    {
        public string? Editorial { get; set; }

        public void GenerateIdentifier()
        {
            Random rnd = new Random();
            String number = rnd.Next(0, 999999).ToString("D13");
            this.Identification = "ISBN " + number;
        }
    }
}
