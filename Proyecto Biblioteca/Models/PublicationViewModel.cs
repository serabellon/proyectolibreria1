﻿namespace Proyecto_Biblioteca.Models
{
    public class PublicationViewModel
    {
        public IEnumerable<Book> Books { get; set; }
        public IEnumerable<Magazine> Magazines { get; set; }
    }
}
