﻿using System.ComponentModel.DataAnnotations;

namespace Proyecto_Biblioteca.Models
{
    public class CreatePublicationModel
    {
        [Required]
        public int PublicationType { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string Category { get; set; }

        public string? Editorial { get; set; }

        public string? Periodicity { get; set; }

        public CreatePublicationModel() { 
            this.PublicationType = 1;
        }
    }
}
