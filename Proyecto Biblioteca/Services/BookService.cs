﻿using Proyecto_Biblioteca.Models;
using Proyecto_Biblioteca.Repositories;

namespace Proyecto_Biblioteca.Services
{
    public class BookService : IBookService
    {
        private readonly IBooksRepository _booksRepository;

        public BookService(IBooksRepository booksRepository)
        {
            _booksRepository = booksRepository;
        }

        public IEnumerable<Book> GetBooks()
        {
            return _booksRepository.GetBooks();
        }

        public void InsertBook(Book book)
        {
            _booksRepository.CreateBook(book);
        }

        public void DeleteBook(Guid bookId)
        {
            _booksRepository.DeleteBook(bookId);
        }
    }
}
