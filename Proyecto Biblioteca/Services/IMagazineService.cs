﻿using Proyecto_Biblioteca.Models;

namespace Proyecto_Biblioteca.Services
{
    public interface IMagazineService
    {
        IEnumerable<Magazine> GetMagazines();
        void InsertMagazine(Magazine book);
        void DeleteMagazine(Guid magazineId);
    }
}
