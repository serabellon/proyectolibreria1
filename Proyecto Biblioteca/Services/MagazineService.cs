﻿using Proyecto_Biblioteca.Models;
using Proyecto_Biblioteca.Repositories;

namespace Proyecto_Biblioteca.Services
{
    public class MagazineService : IMagazineService
    {
        private readonly IMagazinesRepository _magazinesRepository;

        public MagazineService(IMagazinesRepository magazinesRepository)
        {
            _magazinesRepository = magazinesRepository;
        }

        public IEnumerable<Magazine> GetMagazines()
        {
            return _magazinesRepository.GetMagazines();
        }

        public void InsertMagazine(Magazine magazine)
        {
            _magazinesRepository.CreateMagazine(magazine);
        }

        public void DeleteMagazine(Guid magazineId)
        {
            _magazinesRepository.DeleteMagazine(magazineId);
        }
    }
}
