﻿using Proyecto_Biblioteca.Models;

namespace Proyecto_Biblioteca.Services
{
    public interface IBookService
    {
        IEnumerable<Book> GetBooks();
        void InsertBook(Book book);
        void DeleteBook(Guid bookId);
    }
}
