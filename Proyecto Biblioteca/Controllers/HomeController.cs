﻿using Microsoft.AspNetCore.Mvc;
using Proyecto_Biblioteca.Models;
using Proyecto_Biblioteca.Services;
using System.Diagnostics;

namespace Proyecto_Biblioteca.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBookService _bookService;
        private readonly IMagazineService _magazineService;

        public HomeController(ILogger<HomeController> logger, IBookService bookService, IMagazineService magazineService)
        {
            _logger = logger;
            _bookService = bookService;
            _magazineService = magazineService;
        }

        public IActionResult Index()
        {
            var publications = new PublicationViewModel();
            publications.Books = _bookService.GetBooks();
            publications.Magazines = _magazineService.GetMagazines();

            return View(publications);
        }

        public IActionResult CreatePublication()
        {
            return View();
        }

        [HttpPost]
        public IActionResult OnPostCreatePublication(CreatePublicationModel createPublication)
        {
            if (!ModelState.IsValid || createPublication == null) {
                return View("CreatePublication", createPublication);
            }

            if (createPublication.PublicationType == 1)
            {
                var book = new Book();
                book.Title = createPublication.Title;
                book.Author = createPublication.Author;
                book.Category = createPublication.Category;
                book.Editorial = createPublication.Editorial;
                book.GenerateIdentifier();

                _bookService.InsertBook(book);
                ViewBag.Message = "The book was created correctly";
            } else
            {
                var magazine = new Magazine();
                magazine.Title = createPublication.Title;
                magazine.Author = createPublication.Author;
                magazine.Category = createPublication.Category;
                magazine.Periodicity = createPublication.Periodicity;
                magazine.GenerateIdentifier();

                _magazineService.InsertMagazine(magazine);
                ViewBag.Message = "The magazine was created correctly";
            }

            return View("CreatePublication");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}