﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$('#publicationSelect').change(function () {
    if ($(this).val() == 1) {
        $('.editorialField').removeClass('d-none');
        $('.periodicityField').addClass('d-none');
    } else {
        $('.editorialField').addClass('d-none');
        $('.periodicityField').removeClass('d-none');
    }
})