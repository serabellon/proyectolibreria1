﻿using Microsoft.EntityFrameworkCore;

namespace Proyecto_Biblioteca.Repositories
{
    public abstract class BaseRepository
    {
        private readonly IConfiguration _configuration;

        protected BaseRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected DbContextOptions<InMemoryDBContext> getInMemoryOptions()
        {
            var options = new DbContextOptionsBuilder<InMemoryDBContext>()
                .UseInMemoryDatabase(databaseName: _configuration.GetConnectionString("InMemoryDBName"))
                .Options;

            return options;
        }
    }
}
