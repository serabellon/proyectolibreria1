﻿using Proyecto_Biblioteca.Models;

namespace Proyecto_Biblioteca.Repositories
{
    public interface IBooksRepository
    {
        IEnumerable<Book> GetBooks();
        void CreateBook(Book item);
        void DeleteBook(Guid id);
    }
}
