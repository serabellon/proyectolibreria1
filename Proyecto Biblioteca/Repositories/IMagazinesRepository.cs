﻿using Proyecto_Biblioteca.Models;

namespace Proyecto_Biblioteca.Repositories
{
    public interface IMagazinesRepository
    {
        IEnumerable<Magazine> GetMagazines();
        void CreateMagazine(Magazine item);
        void DeleteMagazine(Guid id);
    }
}
