﻿using Proyecto_Biblioteca.Models;

namespace Proyecto_Biblioteca.Repositories
{
    public class BooksRepository : BaseRepository, IBooksRepository
    {
        private InMemoryDBContext _dbContext;

        public BooksRepository(IConfiguration configuration, InMemoryDBContext dbContext) : base(configuration)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Book> GetBooks() { 
            return _dbContext.Books;
        }

        public void CreateBook(Book item)
        {
            var book = new Book();
            book.Id = item.Id;
            book.Title = item.Title;
            book.Author = item.Author;
            book.Category = item.Category;
            book.Editorial = item.Editorial;
            book.GenerateIdentifier();

            _dbContext.Books.Add(book);
            _dbContext.SaveChanges();
        }

        public void DeleteBook(Guid id) { 
            var book = GetBook(id);
            _dbContext.Books.Remove(book);
            _dbContext.SaveChanges();
        }

        private Book GetBook(Guid id)
        {
            var book = _dbContext.Books.Find(id);
            
            if (book == null) throw new KeyNotFoundException("Book not found");

            return book;
        }
    }
}
