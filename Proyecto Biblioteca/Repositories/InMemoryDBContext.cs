﻿using Microsoft.EntityFrameworkCore;
using Proyecto_Biblioteca.Models;

namespace Proyecto_Biblioteca.Repositories
{
    public class InMemoryDBContext: DbContext
    {
        public InMemoryDBContext(DbContextOptions<InMemoryDBContext> options) : base (options) { }

        public DbSet<Magazine> Magazines { get; set; }
        public DbSet<Book> Books { get; set; }
    }
}
