﻿using Proyecto_Biblioteca.Models;

namespace Proyecto_Biblioteca.Repositories
{
    public class MagazinesRepository : BaseRepository, IMagazinesRepository
    {
        private InMemoryDBContext _dbContext;

        public MagazinesRepository(IConfiguration configuration, InMemoryDBContext dbContext) : base(configuration)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Magazine> GetMagazines()
        {
            return _dbContext.Magazines;
        }

        public void CreateMagazine(Magazine item)
        {
            var magazine = new Magazine();
            magazine.Id = item.Id;
            magazine.Title = item.Title;
            magazine.Author = item.Author;
            magazine.Category = item.Category;
            magazine.Periodicity = item.Periodicity;
            magazine.GenerateIdentifier();

            _dbContext.Magazines.Add(magazine);
            _dbContext.SaveChanges();
        }

        public void DeleteMagazine(Guid id)
        {
            var magazine = GetMagazine(id);
            _dbContext.Magazines.Remove(magazine);
            _dbContext.SaveChanges();
        }

        private Magazine GetMagazine(Guid id)
        {
            var magazine = _dbContext.Magazines.Find(id);

            if (magazine == null) throw new KeyNotFoundException("Magazine not found");

            return magazine;
        }
    }
}
